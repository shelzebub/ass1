package ca.bcit.ass1.raymondgollinger_shelylin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class InputActivity extends Activity {

    boolean isDigit;
    boolean showMsg = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

    }


    public void onConvert(View view) {

        // navigate to next page
        Intent navNext = new Intent(this, OutputActivity.class);
        startActivity(navNext);

        Intent convertedValue = new Intent(this, OutputActivity.class);
        EditText textView = (EditText) findViewById(R.id.textBox_input);
        String userInput=textView.getText().toString();

        Intent convertedUnits = new Intent(this, OutputActivity.class);
        EditText textView2 = (EditText) findViewById(R.id.textBox_input);
        String selectedUnits=textView2.getText().toString();

        Spinner mySpinner=(Spinner) findViewById(R.id.spinner_items);
        String textUnits = mySpinner.getSelectedItem().toString();

        // store the user input
        String original = userInput;

        if (userInput.length() == 0) {
            userInput = getResources().getString(R.string.noValueRead);

            selectedUnits = getResources().getString(R.string.tryAgain);
        }

        else if (textUnits.contentEquals(getResources().getString(R.string.kgTolb))) {
            // do kilos to pounds
            userInput = Double.toString(Double.parseDouble(userInput) * 2.2046) + " lb";
            selectedUnits = original + " " + getResources().getString(R.string.kgTolbIs);

        }
        // pounds to kilos
        else if (textUnits.contentEquals(getResources().getString(R.string.lbTokg))) {
            userInput = Double.toString(Double.parseDouble(userInput) / 2.2046) + " kg";
            selectedUnits = original + " " + getResources().getString(R.string.lbTokgIs);

        }
        // kilos to stones
        else if (textUnits.contentEquals(getResources().getString(R.string.kgTost))) {
            userInput = Double.toString(Double.parseDouble(userInput) * 0.15747) + " st";
            selectedUnits = original + " " + getResources().getString(R.string.kgTostIs);

        }
        // stones to kilos
        else if (textUnits.contentEquals(getResources().getString(R.string.stTokg))) {
            userInput = Double.toString(Double.parseDouble(userInput) / 0.15747) + " kg";
            selectedUnits = original + " " + getResources().getString(R.string.stTokgIs);

        }
        // grams to ounces
        else if (textUnits.contentEquals(getResources().getString(R.string.gTooz))) {
            userInput = Double.toString(Double.parseDouble(userInput) * 0.035274) + " oz";
            selectedUnits = original + " " + getResources().getString(R.string.gToozIs);

        }
        // ounces to grams
        else if (textUnits.contentEquals(getResources().getString(R.string.ozTog))) {
            userInput = Double.toString(Double.parseDouble(userInput) / 0.035274) + " g";
            selectedUnits = original + " " + getResources().getString(R.string.ozTogIs);

        }

        // error
        else {
            userInput = getResources().getString(R.string.errorOccured);

            selectedUnits = getResources().getString(R.string.improperUnitSelection);

        }


        //Create the bundle
        Bundle bundle = new Bundle();

        //Add your data to bundle
        bundle.putString("stuff" ,userInput);
        bundle.putString("moreStuff", selectedUnits);

        //Add the bundle to the intent
        convertedValue.putExtras(bundle);
        convertedUnits.putExtras(bundle);


        //Fire that second activity
        startActivity(convertedValue);

        //Get that third one started too
        startActivity(convertedUnits);


    }


}

