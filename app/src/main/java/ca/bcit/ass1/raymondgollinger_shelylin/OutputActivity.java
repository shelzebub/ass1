package ca.bcit.ass1.raymondgollinger_shelylin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class OutputActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_output);

        Intent mIntent = getIntent();
        int intValue = mIntent.getIntExtra("intVariableName", 0);

        // get the bundle
        Bundle bundle = getIntent().getExtras();

        // extract the data…
        String result = bundle.getString("stuff");
        String selectedUnits= bundle.getString("moreStuff");

        //connect result with textview
        TextView res = (TextView) findViewById (R.id.text_result);

        //connect units with textview
        TextView units = (TextView) findViewById (R.id.text_conversion);

        // set result from inputActivity to existing TextView
        res.setText(result);
        units.setText(selectedUnits);

    }

    public void onBack(View view) {
        //intent
        Intent intent = new Intent(this, InputActivity.class);
        startActivity(intent);

    }
    public void onConvert(View view) {

        // navigate to convert page
        Intent intent = new Intent(this, OutputActivity.class);
        startActivity(intent);


    }
}
